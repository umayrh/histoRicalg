Notes 20181011

- Note Peter Selinger about ??rootfinding with CORDIC method which let
   them solve some problem with much less code and # of bits in quantum
   computing.

- Some attention to C and Fortran codes to decide WHICH to document/test/etc.
    -- try to get others to do a code or two
    -- beyond RDS to make it accessible -- maybe CSV

- f90 -- remind Chris (??) about this
      -- select those that may have impact on computations (Are some simply
          housekeeping? e.g., printing)
      -- Can we get someone to document?

- nlm-issues -- essentially resolved
     -- JN to review
     -- code is still converted f2c, and NOT necessarily reviewed.
     -- maybe more tests

- NonlinMin 
     -- reasonably close to finish
     -- review
     -- maybe add other methods, particularly with other contributors
     -- could be person without expertise who asks questions and edits

- Provenance of polyroot
     -- Have cpoly.for "running" but much checked or understoor
     -- Good task for questioner/editor to expand the vignette.

- Provenance of rootfinding
     -- JN and OD to wrap up.
     -- Need to point out that we only do 1D
     -- tests rather than too much code (reference doc. online
        that is referred to
     -- want to tell R users WHAT is best WHEN and give evidence
        to support that
     -- testing template (problem + method(s) --> display of results)
           testroot( probname, start+step OR interval, list of methods)
     -- HWB: R is weak on nD nonlinear equations (rootfinding)
        Homotopy methods are "old" but NOT in R. (Layne Watson)

- RunOldComputingLanguages
     -- fairly good shape as to "how to" vignette 
     -- need to add the algol tests
     -- leads to
          -- securing collections of older codes in machine readable
             http://www.softwarepreservation.org/projects/ALGOL/source/numal/newnumal5p1.txt
          -- Arpad wants PORT3 routines
              JN should contact David Gay
          -- CALGO -- msg Tim Hopkins pending

- specialfun
     -- exponential integral vignette is 60% there. Needs polishing
        Any interested parties (probably in actuarial or similar field)

     -- Bessel approximations might be better here
     -- any others?

- svdpls (Claudia Beleites and JN)
     -- moribund and needs some push
     -- has a start, needs a finish


- templates -- to evolve, but will add some rootfinding stuff,
       including the testbed

- working group -- self explanatory
                -- evolve with activity
                -- after Dal, mailing list broadcast about what's happened
                   with expanded notes and invite to list members to participate more
                -- ask for working group to affirm their interest 
                -- Active members monthly update


Ideas:

   "A guide on how to document ancient code."

Publications beyond Gitlab
   "guide"
   "provenance of rootfinding"


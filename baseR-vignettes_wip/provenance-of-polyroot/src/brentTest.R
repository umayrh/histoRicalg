BrentTest <- function(x){
    if (x == 0) { 
      0
    } else {
      x * exp(1/x^2)
    }
}

#t1 <- uniroot(BrentTest, c(-1, 4))
#t1
# require(rootoned)
#source("./root1d.R")
#t3 <- zeroin(BrentTest, c(-1, 4), trace=FALSE)
#t3
#t2 <- root1d(BrentTest, c(-1, 4), trace=FALSE)
#t2

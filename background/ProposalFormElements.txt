
R Consortium
Submit a Proposal

Primary Contact (Required) John C. Nash

Company/Affiliation (Required) Retired Professor, Telfer School of Management, University of Ottawa

Contact Email (Required) nashjc@uottawa.ca

Contact Phone (Required) 613 831 1656

Title of Project (Required) histoRicalg -- Preserving and Transfering Algorithmic Knowledge

Funding Requested (Required) $10000

Short Description of Project (Required)
histoRicalg aims to preserve knowledge concerning algorithms used in R, particularly the older
methods expressed in programming languages that are now less used. Specifically, we wish

- To establish a "Working Group on Algorithms Used in R" 
to develop a list of target issues and to provide for ongoing
assessment of R's needs for numerical infrastructure.

- To select a small team of older and younger workers, likely one of each, to perform a
prototype exercise in documenting one or more target methods, developing a reference implementation
in R, and reporting on the process. could carry forward the actual work

- From the above, to recommend future steps to preserve knowledge in the R family.

Additional Comments (Required)

The ideas in the proposal have been discussed with a number of experienced R users and developers.
The author of the proposal has been involved in mathematical software for half a century and some
of his contributions are in base R as well as packages. A full resume can be provided on request.


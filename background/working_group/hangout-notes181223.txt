HistoRicalg "hangout" notes for 2018-12-23

On-line: John, Avraham, Arpad, Benoit

Some ideas mentioned:

- Arpad still waiting for acceptance of FOSDEM presentation.

- PORT library merge of JN files with Netlib ones leaves just
a few routines missing. But many do not have tests, though they
do seem to compile. Arpad to send list, John to touch base with
David Gay asking about missing items. May want to add some 
tests.

- Missing routines include some for banded matrices. JN  noted
that one of the codes he wanted from Wilkinson and Reinsch (1971)
is for banded matrices. Reinsch had an email, but has not responded.
Code may need to be manually entered, but may be easier to just
convert to another language. Arpad found Bairstow's method did
not OCR.

- Some discussion of different optimization solvers. AA pointed 
out that short but focused guidance is needed. JN agreed and 
added review of such materials to "to do". 

- John mentioned Charlottesville and possible Sarasota presentations.


!   Output from Public domain Ratfor, version 1.0
    Subroutine sinerp(abd,ld4,nk,p1ip,p2ip,ldnk,flag)
!
!     Purpose :  Computes Inner Products between columns of L^{-1}
!            where L = abd is a Banded Matrix with 3 subdiagonals

!     In R, only called in one place, from sslrvg() [ ./sslvrg.f ] with flag = 0

!     The algorithm works in two passes:
!
!               Pass 1 computes (cj,ck) k=j,j-1,j-2,j-3 ;  j=nk, .. 1
!               Pass 2 computes (cj,ck) k <= j-4  (If flag == 1 ).
!
!               A refinement of Elden's trick is used.
!     Args
!     Locals

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: flag, ld4, ldnk, nk
!     .. Array Arguments ..
      Real (Kind=wp)                   :: abd(ld4,nk), p1ip(ld4,nk),           &
                                          p2ip(ldnk,nk)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: c0, c1, c2, c3
      Integer                          :: i, j, k
!     .. Local Arrays ..
      Real (Kind=wp)                   :: wjm1(1), wjm2(2), wjm3(3)
!     .. Executable Statements ..
!
!     unnecessary initialization of c1 c2 c3 to keep g77 -Wall happy
!
      c1 = 0.0E0_wp
      c2 = 0.0E0_wp
      c3 = 0.0E0_wp
!
!     Pass 1
      wjm3(1) = 0E0_wp
      wjm3(2) = 0E0_wp
      wjm3(3) = 0E0_wp
      wjm2(1) = 0E0_wp
      wjm2(2) = 0E0_wp
      wjm1(1) = 0E0_wp
      Do i = 1, nk
        j = nk - i + 1
        c0 = 1E0_wp/abd(4,j)
        If (j<=nk-3) Then
          c1 = abd(1,j+3)*c0
          c2 = abd(2,j+2)*c0
          c3 = abd(3,j+1)*c0
        Else If (j==nk-2) Then
          c1 = 0E0_wp
          c2 = abd(2,j+2)*c0
          c3 = abd(3,j+1)*c0
        Else If (j==nk-1) Then
          c1 = 0E0_wp
          c2 = 0E0_wp
          c3 = abd(3,j+1)*c0
        Else If (j==nk) Then
          c1 = 0E0_wp
          c2 = 0E0_wp
          c3 = 0E0_wp
        End If
        p1ip(1,j) = 0E0_wp - (c1*wjm3(1)+c2*wjm3(2)+c3*wjm3(3))
        p1ip(2,j) = 0E0_wp - (c1*wjm3(2)+c2*wjm2(1)+c3*wjm2(2))
        p1ip(3,j) = 0E0_wp - (c1*wjm3(3)+c2*wjm2(2)+c3*wjm1(1))
        p1ip(4,j) = c0**2 + c1**2*wjm3(1) + 2E0_wp*c1*c2*wjm3(2) +             &
          2E0_wp*c1*c3*wjm3(3) + c2**2*wjm2(1) + 2E0_wp*c2*c3*wjm2(2) +        &
          c3**2*wjm1(1)
        wjm3(1) = wjm2(1)
        wjm3(2) = wjm2(2)
        wjm3(3) = p1ip(2,j)
        wjm2(1) = wjm1(1)
        wjm2(2) = p1ip(3,j)
        wjm1(1) = p1ip(4,j)
      End Do

      If (flag/=0) Then

!       ____ Pass 2 _____  Compute p2ip  [never from R's code!]
        Do i = 1, nk
          j = nk - i + 1
!           for(k=1;k<=4 & j+k-1<=nk;k=k+1) { p2ip(.) = .. }:
          Do k = 1, 4
            If (j+k-1>nk) Go To 100
            p2ip(j,j+k-1) = p1ip(5-k,j)
          End Do
100     End Do

        Do i = 1, nk
          j = nk - i + 1
!           for(k=j-4;k>=1;k=k-1){
          If (j-4>=1) Then
            Do k = j - 4, 1, -1
              c0 = 1E0_wp/abd(4,k)
              c1 = abd(1,k+3)*c0
              c2 = abd(2,k+2)*c0
              c3 = abd(3,k+1)*c0
              p2ip(k,j) = 0E0_wp - (c1*p2ip(k+3,j)+c2*p2ip(k+2,j)+c3*p2ip(k+1, &
                j))
            End Do
          End If
        End Do
      End If
      Return
    End Subroutine sinerp

!
!     Modified from the SMART package by J.H. Friedman, 10/10/84
!     Main change is to add spline smoothing modified from BRUTO,
!     calling code written for smooth.spline in S.
!
!     B.D. Ripley (ripley@stats.ox.ac.uk)  1994-7.
!
!
    Subroutine smart(m,mu,p,q,n,w,x,y,ww,smod,nsmod,sp,nsp,dp,ndp,edf)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: m, mu, n, ndp, nsmod, nsp, p, q
!     .. Array Arguments ..
      Real (Kind=wp)                   :: dp(ndp), edf(m), smod(nsmod),        &
                                          sp(nsp), w(n), ww(q), x(p,n), y(q,n)
!     .. External Procedures ..
      External                         :: smart1
!     .. Executable Statements ..
      smod(1) = m
      smod(2) = p
      smod(3) = q
      smod(4) = n
      Call smart1(m,mu,p,q,n,w,x,y,ww,smod(6),smod(q+6),smod(q+7),             &
        smod(q+7+p*m),smod(q+7+m*(p+q)),smod(q+7+m*(p+q+n)),smod(q+7+m*(p+q+   &
        2*n)),sp,sp(q*n+1),sp(n*(q+15)+1),sp(n*(q+15)+q+1),dp,smod(5),edf)
      Return
    End Subroutine smart

    Subroutine smart1(m,mu,p,q,n,w,x,y,ww,yb,ys,a,b,f,t,asr,r,sc,bt,g,dp,flm,  &
      edf)

!                        ^^^ really (ndb) of  smart(.)
!     Common Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: flm, ys
      Integer                          :: m, mu, n, p, q
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(p,m), asr(15), b(q,m), bt(q),      &
                                          dp(*), edf(m), f(n,m), g(p,3),       &
                                          r(q,n), sc(n,15), t(n,m), w(n),      &
                                          ww(q), x(p,n), y(*), yb(q)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: asr1, s, sw
      Integer                          :: i, j, l, lm
!     .. External Procedures ..
      External                         :: fulfit, sort, subfit
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, int, sqrt
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, cjeps, conv, cutmin,     &
                                          fdel, span
      Integer                          :: ifl, lf, maxit, mitcj, mitone
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
!     .. Executable Statements ..

      sw = 0E0_wp
      Do j = 1, n
        sw = sw + w(j)
      End Do
      Do j = 1, n
        Do i = 1, q
          r(i,j) = y(q*(j-1)+i)
        End Do
      End Do
      Do i = 1, q
        s = 0E0_wp
        Do j = 1, n
          s = s + w(j)*r(i,j)
        End Do
        yb(i) = s/sw
      End Do
!     yb is vector of means
      Do j = 1, n
        Do i = 1, q
          r(i,j) = r(i,j) - yb(i)
        End Do
      End Do
      ys = 0.E0_wp
      Do i = 1, q
        s = 0.E0_wp
        Do j = 1, n
          s = s + w(j)*r(i,j)**2
        End Do
        ys = ys + ww(i)*s/sw
      End Do
      If (ys>0E0_wp) Go To 100
!     ys is the overall standard deviation -- quit if zero
      Return

100   Continue
      ys = sqrt(ys)
      s = 1.E0_wp/ys
      Do j = 1, n
        Do i = 1, q
          r(i,j) = r(i,j)*s
        End Do
      End Do

!     r is now standardized residuals
!     subfit adds up to m  terms one at time; lm is the number fitted.
      Call subfit(m,p,q,n,w,sw,x,r,ww,lm,a,b,f,t,asr(1),sc,bt,g,dp,edf)
      If (lf<=0) Go To 130
      Call fulfit(lm,lf,p,q,n,w,sw,x,r,ww,a,b,f,t,asr,sc,bt,g,dp,edf)
!     REPEAT
110   Continue
      Do l = 1, lm
        sc(l,1) = l + 0.1E0_wp
        s = 0E0_wp
        Do i = 1, q
          s = s + ww(i)*abs(b(i,l))
        End Do
        sc(l,2) = -s
      End Do
      Call sort(sc(1,2),sc,1,lm)
      Do j = 1, n
        Do i = 1, q
          r(i,j) = y(q*(j-1)+i)
        End Do
      End Do

      Do i = 1, q
        Do j = 1, n
          r(i,j) = r(i,j) - yb(i)
          s = 0.E0_wp
          Do l = 1, lm
            s = s + b(i,l)*f(j,l)
          End Do
          r(i,j) = r(i,j)/ys - s
        End Do
      End Do

      If (lm<=mu) Go To 130
!     back to integer:
      l = int(sc(lm,1))
      asr1 = 0E0_wp
      Do j = 1, n
        Do i = 1, q
          r(i,j) = r(i,j) + b(i,l)*f(j,l)
          asr1 = asr1 + w(j)*ww(i)*r(i,j)**2
        End Do
      End Do

      asr1 = asr1/sw
      asr(1) = asr1
      If (l>=lm) Go To 120
      Do i = 1, p
        a(i,l) = a(i,lm)
      End Do
      Do i = 1, q
        b(i,l) = b(i,lm)
      End Do
      Do j = 1, n
        f(j,l) = f(j,lm)
        t(j,l) = t(j,lm)
      End Do

120   Continue
      lm = lm - 1
      Call fulfit(lm,lf,p,q,n,w,sw,x,r,ww,a,b,f,t,asr,sc,bt,g,dp,edf)
      Go To 110
!     END REPEAT
130   Continue
      flm = lm
      Return
    End Subroutine smart1

    Subroutine subfit(m,p,q,n,w,sw,x,r,ww,lm,a,b,f,t,asr,sc,bt,g,dp,edf)
!     Args
!     Var
!     Common Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: sw
      Integer                          :: lm, m, n, p, q
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(p,m), asr(15), b(q,m), bt(q),      &
                                          dp(*), edf(m), f(n,m), g(p,3),       &
                                          r(q,n), sc(n,15), t(n,m), w(n),      &
                                          ww(q), x(p,n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: asrold
      Integer                          :: i, iflsv, j, l
!     .. External Procedures ..
      External                         :: fulfit, newb, onetrm, rchkusr
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, cjeps, conv, cutmin,     &
                                          fdel, span
      Integer                          :: ifl, lf, maxit, mitcj, mitone
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
!     .. Executable Statements ..

      asr(1) = big
      lm = 0
      Do l = 1, m
        Call rchkusr
        lm = lm + 1
        asrold = asr(1)
        Call newb(lm,q,ww,b)
!       does 'edf' mean 'edf(1)' or 'edf(l)'?
        Call onetrm(0,p,q,n,w,sw,x,r,ww,a(1,lm),b(1,lm),f(1,lm),t(1,lm),       &
          asr(1),sc,g,dp,edf(1))
        Do j = 1, n
          Do i = 1, q
            r(i,j) = r(i,j) - b(i,lm)*f(j,lm)
          End Do
        End Do
        If (lm==1) Go To 100
        If (lf>0) Then
          If (lm==m) Return
          iflsv = ifl
          ifl = 0
          Call fulfit(lm,1,p,q,n,w,sw,x,r,ww,a,b,f,t,asr,sc,bt,g,dp,edf)
          ifl = iflsv
        End If
        If (asr(1)<=0E0_wp .Or. (asrold-asr(1))/asrold<conv) Return
100   End Do
      Return
    End Subroutine subfit

    Subroutine fulfit(lm,lbf,p,q,n,w,sw,x,r,ww,a,b,f,t,asr,sc,bt,g,dp,edf)
!     Args
!     Var
!     Common Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: sw
      Integer                          :: lbf, lm, n, p, q
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(p,lm), asr(1+lm), b(q,lm), bt(q),  &
                                          dp(*), edf(lm), f(n,lm), g(p,3),     &
                                          r(q,n), sc(n,15), t(n,lm), w(n),     &
                                          ww(q), x(p,n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: asri, asrold, fsv
      Integer                          :: i, isv, iter, j, lp
!     .. External Procedures ..
      External                         :: onetrm
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, cjeps, conv, cutmin,     &
                                          fdel, span
      Integer                          :: ifl, lf, maxit, mitcj, mitone
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
!     .. Executable Statements ..

      If (lbf<=0) Return
      asri = asr(1)
      fsv = cutmin
      isv = mitone
      If (lbf<3) Then
        cutmin = 1E0_wp
        mitone = lbf - 1
      End If
      iter = 0
!     Outer loop:
100   Continue
      asrold = asri
      iter = iter + 1
      Do lp = 1, lm
        Do i = 1, q
          bt(i) = b(i,lp)
        End Do
        Do i = 1, p
          g(i,3) = a(i,lp)
        End Do
        Do j = 1, n
          Do i = 1, q
            r(i,j) = r(i,j) + bt(i)*f(j,lp)
          End Do
        End Do

        Call onetrm(1,p,q,n,w,sw,x,r,ww,g(1,3),bt,sc(1,14),sc(1,15),asri,sc,g, &
          dp,edf(lp))
        If (asri<asrold) Then
          Do i = 1, q
            b(i,lp) = bt(i)
          End Do
          Do i = 1, p
            a(i,lp) = g(i,3)
          End Do
          Do j = 1, n
            f(j,lp) = sc(j,14)
            t(j,lp) = sc(j,15)
          End Do
        Else
          asri = asrold
        End If
        Do j = 1, n
          Do i = 1, q
            r(i,j) = r(i,j) - b(i,lp)*f(j,lp)
          End Do
        End Do
      End Do
      If ((iter<=maxit) .And. ((asri>0E0_wp) .And. ((asrold-asri)/asrold>=conv &
        ))) Go To 100
      cutmin = fsv
      mitone = isv
      If (ifl>0) Then
        asr(1+lm) = asri
        asr(1) = asri
      End If
      Return
    End Subroutine fulfit

    Subroutine onetrm(jfl,p,q,n,w,sw,x,y,ww,a,b,f,t,asr,sc,g,dp,edf)
!     Args
!     Var
!     Common Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: asr, edf, sw
      Integer                          :: jfl, n, p, q
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(p), b(q), dp(*), f(n), g(p,2),     &
                                          sc(n,13), t(n), w(n), ww(q), x(p,n), &
                                          y(q,n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: asrold, s
      Integer                          :: i, iter, j
!     .. External Procedures ..
      External                         :: oneone
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, cjeps, conv, cutmin,     &
                                          fdel, span
      Integer                          :: ifl, lf, maxit, mitcj, mitone
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
!     .. Executable Statements ..

      iter = 0
      asr = big
!     REPEAT
100   Continue
      iter = iter + 1
      asrold = asr
      Do j = 1, n
        s = 0E0_wp
        Do i = 1, q
          s = s + ww(i)*b(i)*y(i,j)
        End Do
        sc(j,13) = s
      End Do
      Call oneone(max(jfl,iter-1),p,n,w,sw,sc(1,13),x,a,f,t,asr,sc,g,dp,edf)
      Do i = 1, q
        s = 0E0_wp
        Do j = 1, n
          s = s + w(j)*y(i,j)*f(j)
        End Do
        b(i) = s/sw
      End Do
      asr = 0E0_wp
      Do i = 1, q
        s = 0E0_wp
        Do j = 1, n
          s = s + w(j)*(y(i,j)-b(i)*f(j))**2
        End Do
        asr = asr + ww(i)*s/sw
      End Do
      If ((q/=1) .And. (iter<=maxit) .And. (asr>0E0_wp) .And.                  &
        (asrold-asr)/asrold>=conv) Go To 100
      Return
    End Subroutine onetrm

    Subroutine oneone(ist,p,n,w,sw,y,x,a,f,t,asr,sc,g,dp,edf)
!     Args
!     Var
!     Common Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: asr, edf, sw
      Integer                          :: ist, n, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(p), dp(*), f(n), g(p,2), sc(n,12), &
                                          t(n), w(n), x(p,n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: asrold, cut, s, sml, v
      Integer                          :: i, iter, j, k
!     .. External Procedures ..
      External                         :: pprder, pprdir, sort, supsmu
!     .. Intrinsic Procedures ..
      Intrinsic                        :: int, max, sqrt
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, cjeps, conv, cutmin,     &
                                          fdel, span
      Integer                          :: ifl, lf, maxit, mitcj, mitone
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
!     .. Executable Statements ..

      sml = 1E0_wp/big
      If (ist<=0) Then
        If (p<=1) a(1) = 1E0_wp
        Do j = 1, n
          sc(j,2) = 1E0_wp
        End Do
        Call pprdir(p,n,w,sw,y,x,sc(1,2),a,dp)
      End If
      s = 0E0_wp
      Do i = 1, p
        g(i,1) = 0E0_wp
        s = s + a(i)**2
      End Do
      s = 1E0_wp/sqrt(s)
      Do i = 1, p
        a(i) = a(i)*s
      End Do
      iter = 0
      asr = big
      cut = 1E0_wp
!     REPEAT -----------------------------
100   Continue
      iter = iter + 1
      asrold = asr
!     REPEAT [inner loop] -----
110   Continue
      s = 0E0_wp
      Do i = 1, p
        g(i,2) = a(i) + g(i,1)
        s = s + g(i,2)**2
      End Do
      s = 1.E0_wp/sqrt(s)
      Do i = 1, p
        g(i,2) = g(i,2)*s
      End Do
      Do j = 1, n
        sc(j,1) = j + 0.1E0_wp
        s = 0.E0_wp
        Do i = 1, p
          s = s + g(i,2)*x(i,j)
        End Do
        sc(j,11) = s
      End Do
      Call sort(sc(1,11),sc,1,n)
      Do j = 1, n
        k = int(sc(j,1))
        sc(j,2) = y(k)
        sc(j,3) = max(w(k),sml)
      End Do
      Call supsmu(n,sc(1,11),sc(1,2),sc(1,3),1,span,alpha,sc(1,12),sc(1,4),    &
        edf)
      s = 0E0_wp
      Do j = 1, n
        s = s + sc(j,3)*(sc(j,2)-sc(j,12))**2
      End Do
      s = s/sw
      If (s<asr) Go To 120
      cut = cut*0.5E0_wp
      If (cut<cutmin) Go To 130
      Do i = 1, p
        g(i,1) = g(i,1)*cut
      End Do
      Go To 110
!     --------
120   Continue
      asr = s
      cut = 1E0_wp
      Do i = 1, p
        a(i) = g(i,2)
      End Do
      Do j = 1, n
        k = int(sc(j,1))
        t(k) = sc(j,11)
        f(k) = sc(j,12)
      End Do
      If (asr<=0E0_wp .Or. (asrold-asr)/asrold<conv) Go To 130
      If (iter>mitone .Or. p<=1) Go To 130
      Call pprder(n,sc(1,11),sc(1,12),sc(1,3),fdel,sc(1,4),sc(1,5))
      Do j = 1, n
        k = int(sc(j,1))
        sc(j,5) = y(j) - f(j)
        sc(k,6) = sc(j,4)
      End Do
      Call pprdir(p,n,w,sw,sc(1,5),x,sc(1,6),g,dp)

      Go To 100
!--------------
130   Continue
!--------------
      s = 0E0_wp
      v = s
      Do j = 1, n
        s = s + w(j)*f(j)
      End Do
      s = s/sw
      Do j = 1, n
        f(j) = f(j) - s
        v = v + w(j)*f(j)**2
      End Do
      If (v>0E0_wp) Then
        v = 1E0_wp/sqrt(v/sw)
        Do j = 1, n
          f(j) = f(j)*v
        End Do
      End If
      Return
    End Subroutine oneone


    Subroutine pprdir(p,n,w,sw,r,x,d,e,g)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: sw
      Integer                          :: n, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), e(p), g(*), r(n), w(n), x(p,n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: s
      Integer                          :: i, j, k, l, m1, m2
!     .. External Procedures ..
      External                         :: ppconj
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: cjeps, conv, cutmin, fdel
      Integer                          :: maxit, mitcj, mitone
!     .. Common Blocks ..
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
!     .. Executable Statements ..

      Do i = 1, p
        s = 0E0_wp
        Do j = 1, n
          s = s + w(j)*d(j)*x(i,j)
        End Do
        e(i) = s/sw
      End Do
      k = 0
      m1 = p*(p+1)/2
      m2 = m1 + p
      Do j = 1, p
        s = 0E0_wp
        Do l = 1, n
          s = s + w(l)*r(l)*(d(l)*x(j,l)-e(j))
        End Do
        g(m1+j) = s/sw
        Do i = 1, j
          s = 0E0_wp
          Do l = 1, n
            s = s + w(l)*(d(l)*x(i,l)-e(i))*(d(l)*x(j,l)-e(j))
          End Do
          k = k + 1
          g(k) = s/sw
        End Do
      End Do
      Call ppconj(p,g,g(m1+1),g(m2+1),cjeps,mitcj,g(m2+p+1))
      Do i = 1, p
        e(i) = g(m2+i)
      End Do
      Return
    End Subroutine pprdir

    Subroutine ppconj(p,g,c,x,eps,maxit,sc)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: eps
      Integer                          :: maxit, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: c(p), g(*), sc(p,4), x(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: alpha, beta, h, s, t
      Integer                          :: i, im1, iter, j, nit
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max
!     .. Executable Statements ..

      Do i = 1, p
        x(i) = 0E0_wp
        sc(i,2) = 0E0_wp
      End Do
      nit = 0
!     REPEAT
100   Continue
      nit = nit + 1
      h = 0E0_wp
      beta = 0E0_wp
      Do i = 1, p
        sc(i,4) = x(i)
        s = g(i*(i-1)/2+i)*x(i)
        im1 = i - 1
        j = 1
        Go To 120
110     j = j + 1
120     If (j>im1) Go To 130
        s = s + g(i*(i-1)/2+j)*x(j)
        Go To 110
130     Continue
        j = i + 1
        Go To 150
140     j = j + 1
150     If (j>p) Go To 160
        s = s + g(j*(j-1)/2+i)*x(j)
        Go To 140
160     Continue
        sc(i,1) = s - c(i)
        h = h + sc(i,1)**2
      End Do
      If (h<=0E0_wp) Go To 240
      Do iter = 1, p
        Do i = 1, p
          sc(i,2) = beta*sc(i,2) - sc(i,1)
        End Do
        t = 0E0_wp
        Do i = 1, p
          s = g(i*(i-1)/2+i)*sc(i,2)
          im1 = i - 1
          j = 1
          Go To 180
170       j = j + 1
180       If (j>im1) Go To 190
          s = s + g(i*(i-1)/2+j)*sc(j,2)
          Go To 170
190       Continue
          j = i + 1
          Go To 210
200       j = j + 1
210       If (j>p) Go To 220
          s = s + g(j*(j-1)/2+i)*sc(j,2)
          Go To 200
220       Continue
          sc(i,3) = s
          t = t + s*sc(i,2)
        End Do
        alpha = h/t
        s = 0E0_wp
        Do i = 1, p
          x(i) = x(i) + alpha*sc(i,2)
          sc(i,1) = sc(i,1) + alpha*sc(i,3)
          s = s + sc(i,1)**2
        End Do
        If (s<=0E0_wp) Go To 230
        beta = s/h
        h = s
      End Do
230   Continue
      s = 0E0_wp
      Do i = 1, p
        s = max(s,abs(x(i)-sc(i,4)))
      End Do
      If ((s>=eps) .And. (nit<maxit)) Go To 100
240   Continue
      Return
    End Subroutine ppconj

    Subroutine pprder(n,x,s,w,fdel,d,sc)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fdel
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), s(n), sc(n,3), w(n), x(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: del, scale
      Integer                          :: bc, bl, br, ec, el, er, i, j
!     .. External Procedures ..
      External                         :: pool, rexit
!     .. Executable Statements ..
!
!     unnecessary initialization of bl el ec to keep g77 -Wall happy
!
      bl = 0
      el = 0
      ec = 0
!
      If (x(n)>x(1)) Go To 100
      Do j = 1, n
        d(j) = 0E0_wp
      End Do
      Return
100   Continue
      i = n/4
      j = 3*i
      scale = x(j) - x(i)
110   If (scale>0E0_wp) Go To 120
      If (j<n) j = j + 1
      If (i>1) i = i - 1
      scale = x(j) - x(i)
      Go To 110
120   Continue
      del = fdel*scale*2E0_wp
      Do j = 1, n
        sc(j,1) = x(j)
        sc(j,2) = s(j)
        sc(j,3) = w(j)
      End Do
      Call pool(n,sc,sc(1,2),sc(1,3),del)
      bc = 0
      br = bc
      er = br
130   Continue
      br = er + 1
      er = br
140   If (er>=n) Go To 170
      If (sc(br,1)/=sc(er+1,1)) Go To 150
      er = er + 1
      Go To 160
150   Continue
      Go To 170
160   Continue
      Go To 140
170   Continue
      If (br/=1) Go To 180
      bl = br
      el = er
      Go To 130
180   Continue
      If (bc/=0) Go To 190
      bc = br
      ec = er
      Do j = bl, el
        d(j) = (sc(bc,2)-sc(bl,2))/(sc(bc,1)-sc(bl,1))
      End Do
      Go To 130
190   Continue
!     sanity check needed for PR#13517
      If (br>n) Call rexit('br is too large')
      Do j = bc, ec
        d(j) = (sc(br,2)-sc(bl,2))/(sc(br,1)-sc(bl,1))
      End Do
      If (er/=n) Go To 200
      Do j = br, er
        d(j) = (sc(br,2)-sc(bc,2))/(sc(br,1)-sc(bc,1))
      End Do
      Go To 210
200   Continue
      bl = bc
      el = ec
      bc = br
      ec = er
      Go To 130
210   Continue
      Return
    End Subroutine pprder

    Subroutine pool(n,x,y,w,del)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: del
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: w(n), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: pw, px, py
      Integer                          :: bb, bl, br, eb, el, er, i
!     .. Executable Statements ..

      bb = 0
      eb = bb
100   If (eb>=n) Go To 270
      bb = eb + 1
      eb = bb
110   If (eb>=n) Go To 140
      If (x(bb)/=x(eb+1)) Go To 120
      eb = eb + 1
      Go To 130
120   Continue
      Go To 140
130   Continue
      Go To 110
140   Continue
      If (eb>=n) Go To 200
      If (x(eb+1)-x(eb)>=del) Go To 190
      br = eb + 1
      er = br
150   If (er>=n) Go To 180
      If (x(er+1)/=x(br)) Go To 160
      er = er + 1
      Go To 170
160   Continue
      Go To 180
170   Continue
      Go To 150
180   Continue
!     avoid bounds error: this was .and. but order is not guaranteed
      If (er<n) Then
        If (x(er+1)-x(er)<x(eb+1)-x(eb)) Go To 100
      End If
      eb = er
      pw = w(bb) + w(eb)
      px = (x(bb)*w(bb)+x(eb)*w(eb))/pw
      py = (y(bb)*w(bb)+y(eb)*w(eb))/pw
      Do i = bb, eb
        x(i) = px
        y(i) = py
        w(i) = pw
      End Do
190   Continue
200   Continue
210   Continue
      If (bb<=1) Go To 260
      If (x(bb)-x(bb-1)>=del) Go To 260
      bl = bb - 1
      el = bl
220   If (bl<=1) Go To 250
      If (x(bl-1)/=x(el)) Go To 230
      bl = bl - 1
      Go To 240
230   Continue
      Go To 250
240   Continue
      Go To 220
250   Continue
      bb = bl
      pw = w(bb) + w(eb)
      px = (x(bb)*w(bb)+x(eb)*w(eb))/pw
      py = (y(bb)*w(bb)+y(eb)*w(eb))/pw
      Do i = bb, eb
        x(i) = px
        y(i) = py
        w(i) = pw
      End Do
      Go To 210
260   Continue
      Go To 100
270   Continue
      Return
    End Subroutine pool

    Subroutine newb(lm,q,ww,b)

!     Common

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: lm, q
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(q,lm), ww(q)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: s, sml, t
      Integer                          :: i, l, l1, lm1
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, sqrt
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, span
      Integer                          :: ifl, lf
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
!     .. Executable Statements ..


      sml = 1E0_wp/big
      If (q/=1) Go To 100
      b(1,lm) = 1E0_wp
      Return
100   Continue
      If (lm/=1) Go To 110
      Do i = 1, q
        b(i,lm) = i
      End Do
      Return
110   Continue
      lm1 = lm - 1
      Do i = 1, q
        b(i,lm) = 0E0_wp
      End Do
      t = 0E0_wp
      Do i = 1, q
        s = 0E0_wp
        Do l = 1, lm1
          s = s + abs(b(i,l))
        End Do
        b(i,lm) = s
        t = t + s
      End Do
      Do i = 1, q
        b(i,lm) = ww(i)*(t-b(i,lm))
      End Do
      l1 = 1
      If (lm>q) l1 = lm - q + 1
      Do l = l1, lm1
        s = 0E0_wp
        t = s
        Do i = 1, q
          s = s + ww(i)*b(i,lm)*b(i,l)
          t = t + ww(i)*b(i,l)**2
        End Do
        s = s/sqrt(t)
        Do i = 1, q
          b(i,lm) = b(i,lm) - s*b(i,l)
        End Do
      End Do
      Do i = 2, q
        If (abs(b(i-1,lm)-b(i,lm))>sml) Return
      End Do
      Do i = 1, q
        b(i,lm) = i
      End Do
      Return
    End Subroutine newb

    Block Data bkppr

!     Common Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, cjeps, conv, cutmin, df, &
                                          fdel, gcvpen, span
      Integer                          :: ifl, ismethod, lf, maxit, mitcj,     &
                                          mitone
      Logical                          :: trace
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /pprz01/conv, maxit, mitone, cutmin, fdel, cjeps, mitcj
      Common /spsmooth/df, gcvpen, ismethod, trace
!     .. Data Statements ..
      Data df, gcvpen, ismethod, trace/4E0_wp, 1E0_wp, 0, .False./
      Data ifl, maxit, conv, mitone, cutmin, fdel, span, alpha, big, cjeps,    &
        mitcj, lf/6, 20, .005_wp, 20, 0.1_wp, 0.02_wp, 0.0_wp, 0.0_wp,         &
        1.0E20_wp, 0.001_wp, 1, 2/
    End Block Data bkppr

    Subroutine setppr(span1,alpha1,optlevel,ism,df1,gcvpen1)
!     Put 'parameters' into Common blocks

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: alpha1, df1, gcvpen1, span1
      Integer                          :: ism, optlevel
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: alpha, big, df, gcvpen, span
      Integer                          :: ifl, ismethod, lf
      Logical                          :: trace
!     .. Common Blocks ..
      Common /pprpar/ifl, lf, span, alpha, big
      Common /spsmooth/df, gcvpen, ismethod, trace
!     .. Executable Statements ..

      span = span1
      lf = optlevel
      alpha = alpha1
      If (ism>=0) Then
        ismethod = ism
        trace = .False.
      Else
        ismethod = -(ism+1)
        trace = .True.
      End If
      df = df1
      gcvpen = gcvpen1
      Return
    End Subroutine setppr

    Subroutine fsort(mu,n,f,t,sp)
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: mu, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: f(n,mu), sp(n,2), t(n,mu)
!     .. Local Scalars ..
      Integer                          :: j, k, l
!     .. External Procedures ..
      External                         :: sort
!     .. Intrinsic Procedures ..
      Intrinsic                        :: int
!     .. Executable Statements ..

      Do l = 1, mu
        Do j = 1, n
          sp(j,1) = j + 0.1E0_wp
          sp(j,2) = f(j,l)
        End Do
        Call sort(t(1,l),sp,1,n)
        Do j = 1, n
          k = int(sp(j,1))
          f(j,l) = sp(k,2)
        End Do
      End Do
      Return
    End Subroutine fsort

    Subroutine pppred(np,x,smod,y,sc)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: np
!     .. Array Arguments ..
      Real (Kind=wp)                   :: sc(*), smod(*), x(np,*), y(np,*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: s, t, ys
      Integer                          :: high, i, inp, j, ja, jb, jf, jfh,    &
                                          jfl, jt, jth, jtl, l, low, m, mu, n, &
                                          p, place, q
!     .. External Procedures ..
      External                         :: fsort
!     .. Intrinsic Procedures ..
      Intrinsic                        :: int
!     .. Executable Statements ..

      m = int(smod(1)+0.1E0_wp)
      p = int(smod(2)+0.1E0_wp)
      q = int(smod(3)+0.1E0_wp)
      n = int(smod(4)+0.1E0_wp)
      mu = int(smod(5)+0.1E0_wp)
      ys = smod(q+6)
      ja = q + 6
      jb = ja + p*m
      jf = jb + m*q
      jt = jf + n*m
      Call fsort(mu,n,smod(jf+1),smod(jt+1),sc)
      Do inp = 1, np
        ja = q + 6
        jb = ja + p*m
        jf = jb + m*q
        jt = jf + n*m
        Do i = 1, q
          y(inp,i) = 0E0_wp
        End Do
        Do l = 1, mu
          s = 0E0_wp
          Do j = 1, p
            s = s + smod(ja+j)*x(inp,j)
          End Do
          If (s>smod(jt+1)) Go To 100
          place = 1
          Go To 140
100       Continue
          If (s<smod(jt+n)) Go To 110
          place = n
          Go To 140

110       Continue
          low = 0
          high = n + 1
!         WHILE
120       If (low+1>=high) Go To 130
          place = (low+high)/2
          t = smod(jt+place)
          If (s==t) Go To 140
          If (s<t) Then
            high = place
          Else
            low = place
          End If
          Go To 120
!         END
130       Continue
          jfl = jf + low
          jfh = jf + high
          jtl = jt + low
          jth = jt + high
          t = smod(jfl) + (smod(jfh)-smod(jfl))*(s-smod(jtl))/(smod(jth)-smod( &
            jtl))
          Go To 150
140       Continue
          t = smod(jf+place)
150       Continue
          Do i = 1, q
            y(inp,i) = y(inp,i) + smod(jb+i)*t
          End Do
          ja = ja + p
          jb = jb + q
          jf = jf + n
          jt = jt + n
        End Do
        Do i = 1, q
          y(inp,i) = ys*y(inp,i) + smod(i+5)
        End Do
      End Do
      Return
    End Subroutine pppred

!   Called from R's supsmu()
    Subroutine setsmu(tr)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: tr
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: df, gcvpen
      Integer                          :: ismethod
      Logical                          :: trace
!     .. Common Blocks ..
      Common /spsmooth/df, gcvpen, ismethod, trace
!     .. Executable Statements ..

      ismethod = 0
      trace = tr /= 0
      Return
    End Subroutine setsmu

    Subroutine supsmu(n,x,y,w,iper,span,alpha,smo,sc,edf)
!
!------------------------------------------------------------------
!
!     super smoother (Friedman, 1984).
!
!     version 10/10/84
!
!     coded  and copywrite (c) 1984 by:
!
!                        Jerome H. Friedman
!                     department of statistics
!                               and
!                stanford linear accelerator center
!                        stanford university
!
!     all rights reserved.
!
!
!     input:
!     n : number of observations (x,y - pairs).
!     x(n) : ordered abscissa values.
!     y(n) : corresponding ordinate (response) values.
!     w(n) : weight for each (x,y) observation.
!     iper : periodic variable flag.
!       iper=1 => x is ordered interval variable.
!       iper=2 => x is a periodic variable with values
!                 in the range (0.0,1.0) and period 1.0.
!     span : smoother span (fraction of observations in window).
!           span=0.0 <=> "cv" : automatic (variable) span selection.
!     alpha : controls high frequency (small span) penality
!            used with automatic span selection (bass tone control).
!            (alpha.le.0.0 or alpha.gt.10.0 => no effect.)
!     output:
!     smo(n) : smoothed ordinate (response) values.
!     scratch:
!     sc(n,7) : internal working storage.
!
!     note:
!     for small samples (n < 40) or if there are substantial serial
!     correlations between observations close in x - value, then
!     a prespecified fixed span smoother (span > 0) should be
!     used. reasonable span values are 0.2 to 0.4.
!
!------------------------------------------------------------------

!     Args
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: alpha, edf, span
      Integer                          :: iper, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: sc(n,7), smo(n), w(n), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, f, resmin, scale, sw, sy, vsmlsq
      Integer                          :: i, j, jper
!     .. Local Arrays ..
      Real (Kind=wp)                   :: h(n)
!     .. External Procedures ..
      External                         :: smooth, spline
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: big, df, eps, gcvpen, sml
      Integer                          :: ismethod
      Logical                          :: trace
!     .. Arrays in Common ..
      Real (Kind=wp)                   :: spans(3)
!     .. Common Blocks ..
      Common /consts/big, sml, eps
      Common /spans/spans
      Common /spsmooth/df, gcvpen, ismethod, trace
!     .. Executable Statements ..
!     Called from R's supsmu(),  ismethod = 0, always (but not when called from ppr)

      If (x(n)>x(1)) Go To 100
!     x(n) <= x(1) :  boundary case:  smo[.] :=  weighted mean( y )
      sy = 0E0_wp
      sw = sy
      Do j = 1, n
        sy = sy + w(j)*y(j)
        sw = sw + w(j)
      End Do
      a = 0E0_wp
      If (sw>0E0_wp) a = sy/sw
      Do j = 1, n
        smo(j) = a
      End Do
      Return

!     Normal Case
100   Continue
      If (ismethod/=0) Then ! possible only when called from ppr()
        Call spline(n,x,y,w,smo,edf,sc)
      Else
        i = n/4
        j = 3*i
        scale = x(j) - x(i) ! = IQR(x)
110     If (scale>0E0_wp) Go To 120
        If (j<n) j = j + 1
        If (i>1) i = i - 1
        scale = x(j) - x(i)
        Go To 110
120     vsmlsq = (eps*scale)**2
        jper = iper
        If (iper==2 .And. (x(1)<0E0_wp .Or. x(n)>1E0_wp)) jper = 1
        If (jper<1 .Or. jper>2) jper = 1
        If (span>0E0_wp) Then
          Call smooth(n,x,y,w,span,jper,vsmlsq,smo,sc)
          Return
        End If
!       else  "cv" (crossvalidation) from  three spans[]
        Do i = 1, 3
          Call smooth(n,x,y,w,spans(i),jper,vsmlsq,sc(1,2*i-1),sc(1,7))
          Call smooth(n,x,sc(1,7),w,spans(2),-jper,vsmlsq,sc(1,2*i),h)
        End Do
        Do j = 1, n
          resmin = big
          Do i = 1, 3
            If (sc(j,2*i)>=resmin) Go To 130
            resmin = sc(j,2*i)
            sc(j,7) = spans(i)
130       End Do
          If (alpha>0E0_wp .And. alpha<=10E0_wp .And. resmin<sc(j,6) .And.     &
            resmin>0E0_wp) sc(j,7) = sc(j,7) + (spans(3)-sc(j,7))*max(sml,     &
            resmin/sc(j,6))**(10E0_wp-alpha)
        End Do

        Call smooth(n,x,sc(1,7),w,spans(2),-jper,vsmlsq,sc(1,2),h)
        Do j = 1, n
          If (sc(j,2)<=spans(1)) sc(j,2) = spans(1)
          If (sc(j,2)>=spans(3)) sc(j,2) = spans(3)
          f = sc(j,2) - spans(2)
          If (f>=0E0_wp) Go To 140
          f = -f/(spans(2)-spans(1))
          sc(j,4) = (1E0_wp-f)*sc(j,3) + f*sc(j,1)
          Go To 150
140       f = f/(spans(3)-spans(2))
          sc(j,4) = (1E0_wp-f)*sc(j,3) + f*sc(j,5)
150     End Do
        Call smooth(n,x,sc(1,4),w,spans(1),-jper,vsmlsq,smo,h)
        edf = 0
      End If
      Return
    End Subroutine supsmu

    Subroutine smooth(n,x,y,w,span,iper,vsmlsq,smo,acvr)
!     Args
!     Var

!     will use 'trace':

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: span, vsmlsq
      Integer                          :: iper, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: acvr(n), smo(n), w(n), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, cvar, fbo, fbw, h, sy, tmp, var,  &
                                          wt, xm, xti, xto, ym
      Integer                          :: i, ibw, in, it, j, j0, jper, out
!     .. External Procedures ..
      External                         :: smoothprt
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, int
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: df, gcvpen
      Integer                          :: ismethod
      Logical                          :: trace
!     .. Common Blocks ..
      Common /spsmooth/df, gcvpen, ismethod, trace
!     .. Executable Statements ..

      xm = 0E0_wp
      ym = xm
      var = ym
      cvar = var
      fbw = cvar
      jper = abs(iper)
      ibw = int(0.5E0_wp*span*n+0.5E0_wp)
      If (ibw<2) ibw = 2
      it = 2*ibw + 1
      If (it>n) it = n
      Do i = 1, it
        j = i
        If (jper==2) j = i - ibw - 1
        If (j>=1) Then
          xti = x(j)
        Else ! if (j.lt.1) then
          j = n + j
          xti = x(j) - 1E0_wp
        End If
        wt = w(j)
        fbo = fbw
        fbw = fbw + wt
        If (fbw>0E0_wp) xm = (fbo*xm+wt*xti)/fbw
        If (fbw>0E0_wp) ym = (fbo*ym+wt*y(j))/fbw
        tmp = 0E0_wp
        If (fbo>0E0_wp) tmp = fbw*wt*(xti-xm)/fbo
        var = var + tmp*(xti-xm)
        cvar = cvar + tmp*(y(j)-ym)
      End Do

      Do j = 1, n
        out = j - ibw - 1
        in = j + ibw
        If ((jper/=2) .And. (out<1 .Or. in>n)) Go To 100
        If (out<1) Then
          out = n + out
          xto = x(out) - 1E0_wp
          xti = x(in)
        Else If (in>n) Then
          in = in - n
          xti = x(in) + 1E0_wp
          xto = x(out)
        Else
          xto = x(out)
          xti = x(in)
        End If
        wt = w(out)
        fbo = fbw
        fbw = fbw - wt
        tmp = 0E0_wp
        If (fbw>0E0_wp) tmp = fbo*wt*(xto-xm)/fbw
        var = var - tmp*(xto-xm)
        cvar = cvar - tmp*(y(out)-ym)
        If (fbw>0E0_wp) xm = (fbo*xm-wt*xto)/fbw
        If (fbw>0E0_wp) ym = (fbo*ym-wt*y(out))/fbw
        wt = w(in)
        fbo = fbw
        fbw = fbw + wt
        If (fbw>0E0_wp) xm = (fbo*xm+wt*xti)/fbw
        If (fbw>0E0_wp) ym = (fbo*ym+wt*y(in))/fbw
        tmp = 0E0_wp
        If (fbo>0E0_wp) tmp = fbw*wt*(xti-xm)/fbo
        var = var + tmp*(xti-xm)
        cvar = cvar + tmp*(y(in)-ym)
100     a = 0E0_wp
        If (var>vsmlsq) a = cvar/var
        smo(j) = a*(x(j)-xm) + ym
        If (iper>0) Then
          h = 0E0_wp
          If (fbw>0E0_wp) h = 1E0_wp/fbw
          If (var>vsmlsq) h = h + (x(j)-xm)**2/var
          acvr(j) = 0E0_wp
          a = 1E0_wp - w(j)*h
          If (a>0E0_wp) Then
            acvr(j) = abs(y(j)-smo(j))/a
          Else If (j>1) Then
            acvr(j) = acvr(j-1)
          End If
        End If
      End Do

      If (trace) Call smoothprt(span,iper,var,cvar) ! -> ./ksmooth.c

!     -- Recompute fitted values smo(j) as weighted mean for non-unique x(.) values:
      j = 1
110   j0 = j
      sy = smo(j)*w(j)
      fbw = w(j)
      If (j>=n) Go To 130
120   If (x(j+1)<=x(j)) Then
        j = j + 1
        sy = sy + w(j)*smo(j)
        fbw = fbw + w(j)
        If (j<n) Go To 120
      End If
130   If (j>j0) Then
        a = 0E0_wp
        If (fbw>0E0_wp) a = sy/fbw
        Do i = j0, j
          smo(i) = a
        End Do
      End If
      j = j + 1
      If (j<=n) Go To 110
      Return
    End Subroutine smooth


    Block Data bksupsmu

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: big, eps, sml
!     .. Arrays in Common ..
      Real (Kind=wp)                   :: spans(3)
!     .. Common Blocks ..
      Common /consts/big, sml, eps
      Common /spans/spans
!     .. Data Statements ..
      Data spans, big, sml, eps/0.05_wp, 0.2_wp, 0.5_wp, 1.0E20_wp, 1.0E-7_wp, &
        1.0E-3_wp/
    End Block Data bksupsmu
!---------------------------------------------------------------
!
!   this sets the compile time (default) values for various
!   internal parameters :
!
!   spans : span values for the three running linear smoothers.
!   spans(1) : tweeter span.
!   spans(2) : midrange span.
!   spans(3) : woofer span.
!       (these span values should be changed only with care.)
!   big : a large representable floating point number.
!   sml : a small number. should be set so that (sml)**(10.0) does
!       not cause floating point underflow.
!   eps : used to numerically stabilize slope calculations for
!       running linear fits.
!
!   these parameter values can be changed by declaring the
!   relevant labeled common in the main program and resetting
!   them with executable statements.
!

!   Only for  ppr(*, ismethod != 0):  Compute "smoothing" spline
!   (rather, a penalized regression spline with at most 15 (inner) knots):
!-----------------------------------------------------------------
!
    Subroutine spline(n,x,y,w,smo,edf,sc)
!
!------------------------------------------------------------------
!
!     input:
!     n : number of observations.
!     x(n) : ordered abscissa values.
!     y(n) : corresponding ordinate (response) values.
!     w(n) : weight for each (x,y) observation.
!     work space:
!     sc(n,7) : used for dx(n), dy(n), dw(n), dsmo(n), lev(n)
!     output:
!     smo(n) : smoothed ordinate (response) values.
!     edf : equivalent degrees of freedom
!
!------------------------------------------------------------------
!     Args

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: edf
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: sc(n,7), smo(n), w(n), x(n), y(n)
!     .. External Procedures ..
      External                         :: splineaa
!     .. Executable Statements ..

!     dx
!     dy
!     dw
!     dsmo
      Call splineaa(n,x,y,w,smo,edf,sc(n,1),sc(n,2),sc(n,3),sc(n,4),sc(n,5)) ! lev

      Return
    End Subroutine spline


    Subroutine splineaa(n,x,y,w,smo,edf,dx,dy,dw,dsmo,lev)
!
!     Workhorse of spline() above
!------------------------------------------------------------------
!
!     Additional input variables (no extra output, work):
!     dx :
!     dy :
!     dw :
!     dsmo:
!     lev : "leverages", i.e., diagonal entries S_{i,i} of the smoother matrix

!
!------------------------------------------------------------------
!     Args
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: edf
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: dsmo(n), dw(n), dx(n), dy(n),        &
                                          lev(n), smo(n), w(n), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: crit, df1, lambda, p, s
      Integer                          :: i, ier, ip, nk
!     .. Local Arrays ..
      Real (Kind=wp)                   :: coef(25), knot(29), param(5),        &
                                          work((17+25)*25)
      Integer                          :: iparms(4)
!     .. External Procedures ..
      External                         :: intpr, rbart, splineprt
!     .. Intrinsic Procedures ..
      Intrinsic                        :: int, min, real
!     .. Scalars in Common ..
      Real (Kind=wp)                   :: df, gcvpen
      Integer                          :: ismethod
      Logical                          :: trace
!     .. Common Blocks ..
      Common /spsmooth/df, gcvpen, ismethod, trace
!     .. Executable Statements ..

!     __no-more__ if (n .gt. 2500) call bdrsplerr()
      Do i = 1, n
        dx(i) = (x(i)-x(1))/(x(n)-x(1))
        dy(i) = y(i)
        dw(i) = w(i)
      End Do
      nk = min(n,15)
      knot(1) = dx(1)
      knot(2) = dx(1)
      knot(3) = dx(1)
      knot(4) = dx(1)
      knot(nk+1) = dx(n)
      knot(nk+2) = dx(n)
      knot(nk+3) = dx(n)
      knot(nk+4) = dx(n)
      Do i = 5, nk
        p = (n-1)*real(i-4,kind=wp)/real(nk-3,kind=wp)
        ip = int(p)
        p = p - ip
        knot(i) = (1-p)*dx(ip+1) + p*dx(ip+2)
      End Do
!      call dblepr('knots', 5, knot, nk+4)
!     iparms(1:2) := (icrit, ispar)  for ./sbart.c
      If (ismethod==1) Then
        iparms(1) = 3
        df1 = df
      Else
        iparms(1) = 1
        df1 = 0E0_wp
      End If
!
      iparms(2) = 0 ! ispar := 0 <==> estimate `spar'
      iparms(3) = 500 ! maxit = 500
      iparms(4) = 0 ! spar (!= lambda)
!
      param(1) = 0E0_wp ! = lspar : min{spar}
      param(2) = 1.5E0_wp ! = uspar : max{spar}
!     tol for 'spar' estimation:
      param(3) = 1E-2_wp
!     'eps' (~= 2^-12 = sqrt(2^-24) ?= sqrt(machine eps)) in ./sbart.c :
      param(4) = .000244_wp

      ier = 1
      Call rbart(gcvpen,df1,dx,dy,dw,0.0E0_wp,n,knot,nk,coef,dsmo,lev,crit,    &
        iparms,lambda,param,work,4,1,ier)
      If (ier>0) Call intpr('spline(.) TROUBLE:',18,ier,1)
      Do i = 1, n
        smo(i) = dsmo(i)
      End Do
!      call dblepr('smoothed',8, dsmo, n)
      s = 0
      Do i = 1, n
        s = s + lev(i)
      End Do
      edf = s
      If (trace) Call splineprt(df,gcvpen,ismethod,lambda,edf)
      Return
    End Subroutine splineaa


!**********************************************************************

!   === This was 'sort()' in  gamfit's  mysort.f  [or sortdi() in sortdi.f ] :
!
!   ===  FIXME:  Translate to C and add to ../../../main/sort.c <<<<<
!
!     a[] is double precision because the caller reuses a double (sometimes v[] itself!)
    Subroutine sort(v,a,ii,jj)
!
!     Puts into a the permutation vector which sorts v into
!     increasing order.  Only elements from ii to jj are considered.
!     Arrays iu(k) and il(k) permit sorting up to 2**(k+1)-1 elements
!
!     This is a modification of CACM algorithm #347 by R. C. Singleton,
!     which is a modified Hoare quicksort.
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ii, jj
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(jj), v(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: vt, vtt
      Integer                          :: i, ij, j, k, l, m, t, tt
!     .. Local Arrays ..
      Integer                          :: il(20), iu(20)
!     .. Intrinsic Procedures ..
      Intrinsic                        :: int
!     .. Executable Statements ..

      m = 1
      i = ii
      j = jj
100   If (i>=j) Go To 170
110   k = i
      ij = (j+i)/2
      t = int(a(ij))
      vt = v(ij)
      If (v(i)<=vt) Go To 120
      a(ij) = a(i)
      a(i) = t
      t = int(a(ij))
      v(ij) = v(i)
      v(i) = vt
      vt = v(ij)
120   l = j
      If (v(j)>=vt) Go To 140
      a(ij) = a(j)
      a(j) = t
      t = int(a(ij))
      v(ij) = v(j)
      v(j) = vt
      vt = v(ij)
      If (v(i)<=vt) Go To 140
      a(ij) = a(i)
      a(i) = t
      t = int(a(ij))
      v(ij) = v(i)
      v(i) = vt
      vt = v(ij)
      Go To 140
130   a(l) = a(k)
      a(k) = tt
      v(l) = v(k)
      v(k) = vtt
140   l = l - 1
      If (v(l)>vt) Go To 140
      tt = int(a(l))
      vtt = v(l)
150   k = k + 1
      If (v(k)<vt) Go To 150
      If (k<=l) Go To 130
      If (l-i<=j-k) Go To 160
      il(m) = i
      iu(m) = l
      i = k
      m = m + 1
      Go To 180
160   il(m) = k
      iu(m) = j
      j = l
      m = m + 1
      Go To 180
170   m = m - 1
      If (m==0) Return
      i = il(m)
      j = iu(m)
180   If (j-i>10) Go To 110
      If (i==ii) Go To 100
      i = i - 1
190   i = i + 1
      If (i==j) Go To 170
      t = int(a(i+1))
      vt = v(i+1)
      If (v(i)<=vt) Go To 190
      k = i
200   a(k+1) = a(k)
      v(k+1) = v(k)
      k = k - 1
      If (vt<v(k)) Go To 200
      a(k+1) = t
      v(k+1) = vt
      Go To 190
    End Subroutine sort

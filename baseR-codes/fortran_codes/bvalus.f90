    Subroutine bvalus(n,knot,coef,nk,x,s,order)
!     Args
!     Local

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n, nk, order
!     .. Array Arguments ..
      Real (Kind=wp)                   :: coef(*), knot(*), s(*), x(*)
!     .. Local Scalars ..
      Integer                          :: i
!     .. External Procedures ..
      Real (Kind=wp), External         :: bvalue
!     .. Executable Statements ..

      Do i = 1, n
        s(i) = bvalue(knot,coef,nk,4,x(i),order)
      End Do
      Return
    End Subroutine bvalus

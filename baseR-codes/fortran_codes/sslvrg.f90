!   Output from Public domain Ratfor, version 1.0

!   Smoothing Spline LeVeRaGes = SSLVRG
!   ----------------------------------- leverages = H_ii = diagonal entries of Hat matrix
    Subroutine sslvrg(penalt,dofoff,x,y,w,ssw,n,knot,nk,coef,sz,lev,crit,      &
      icrit,lambda,xwy,hs0,hs1,hs2,hs3,sg0,sg1,sg2,sg3,abd,p1ip,p2ip,ld4,ldnk, &
      info)

!     Purpose :
!       Compute smoothing spline for smoothing parameter lambda
!       and compute one of three `criteria' (OCV , GCV , "df match").
!     See comments in ./sbart.c from which this is called

!     local variables
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: crit, dofoff, lambda, penalt, ssw
      Integer                          :: icrit, info, ld4, ldnk, n, nk
!     .. Array Arguments ..
      Real (Kind=wp)                   :: abd(ld4,nk), coef(nk), hs0(nk),      &
                                          hs1(nk), hs2(nk), hs3(nk),           &
                                          knot(nk+4), lev(n), p1ip(ld4,nk),    &
                                          p2ip(ldnk,nk), sg0(nk), sg1(nk),     &
                                          sg2(nk), sg3(nk), sz(n), w(n), x(n), &
                                          xwy(nk), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: b0, b1, b2, b3, df, eps, rss, sumw,  &
                                          xv
      Integer                          :: i, ileft, j, lenkno, mflag
!     .. Local Arrays ..
      Real (Kind=wp)                   :: vnikx(4,1), work(16)
!     .. External Procedures ..
      Real (Kind=wp), External         :: bvalue
      Integer, External                :: interv
      External                         :: bsplvd, dpbfa, dpbsl, sinerp
!     .. Executable Statements ..

      lenkno = nk + 4
      ileft = 1
      eps = 1E-11_wp

!     compute the coefficients coef() of estimated smooth

      Do i = 1, nk
        coef(i) = xwy(i)
        abd(4,i) = hs0(i) + lambda*sg0(i)
      End Do

      Do i = 1, (nk-1)
        abd(3,i+1) = hs1(i) + lambda*sg1(i)
      End Do

      Do i = 1, (nk-2)
        abd(2,i+2) = hs2(i) + lambda*sg2(i)
      End Do

      Do i = 1, (nk-3)
        abd(1,i+3) = hs3(i) + lambda*sg3(i)
      End Do

!     factorize banded matrix abd (into upper triangular):
      Call dpbfa(abd,ld4,nk,3,info)
      If (info/=0) Then
!        matrix could not be factorized -> ier := info
        Return
      End If
!     solve linear system (from factorized abd):
      Call dpbsl(abd,ld4,nk,3,coef)

!     Value of smooth at the data points
      Do i = 1, n
        xv = x(i)
        sz(i) = bvalue(knot,coef,nk,4,xv,0)
      End Do

!     Compute the criterion function if requested (icrit > 0) :
      If (icrit>=1) Then

!       --- Ordinary or Generalized CV or "df match" ---

!       Get Leverages First
        Call sinerp(abd,ld4,nk,p1ip,p2ip,ldnk,0)
        Do i = 1, n
          xv = x(i)
          ileft = interv(knot(1),nk+1,xv,0,0,ileft,mflag)
          If (mflag==-1) Then
            ileft = 4
            xv = knot(4) + eps
          Else If (mflag==1) Then
            ileft = nk
            xv = knot(nk+1) - eps
          End If
          j = ileft - 3
!           call bspvd(knot,4,1,xv,ileft,4,vnikx,work)
          Call bsplvd(knot,lenkno,4,xv,ileft,work,vnikx,1)
          b0 = vnikx(1,1)
          b1 = vnikx(2,1)
          b2 = vnikx(3,1)
          b3 = vnikx(4,1)
          lev(i) = (p1ip(4,j)*b0**2+2.E0_wp*p1ip(3,j)*b0*b1+2.E0_wp*p1ip(2,j)* &
            b0*b2+2.E0_wp*p1ip(1,j)*b0*b3+p1ip(4,j+1)*b1**2+                   &
            2.E0_wp*p1ip(3,j+1)*b1*b2+2.E0_wp*p1ip(2,j+1)*b1*b3+               &
            p1ip(4,j+2)*b2**2+2.E0_wp*p1ip(3,j+2)*b2*b3+p1ip(4,j+3)*b3**2)*    &
            w(i)**2
        End Do


!       Evaluate Criterion

        df = 0E0_wp
        If (icrit==1) Then ! Generalized CV --------------------
          rss = ssw
          sumw = 0E0_wp
!         w(i) are sqrt( wt[i] ) weights scaled in ../R/smspline.R such
!         that sumw =  number of observations with w(i) > 0
          Do i = 1, n
            rss = rss + ((y(i)-sz(i))*w(i))**2
            df = df + lev(i)
            sumw = sumw + w(i)**2
          End Do

          crit = (rss/sumw)/((1E0_wp-(dofoff+penalt*df)/sumw)**2)
!            call dblepr("spar", 4, spar, 1)
!            call dblepr("crit", 4, crit, 1)

        Else If (icrit==2) Then ! Ordinary CV ------------------
          crit = 0E0_wp
          Do i = 1, n
            crit = crit + (((y(i)-sz(i))*w(i))/(1-lev(i)))**2
          End Do
          crit = crit/n
!            call dblepr("spar", 4, spar, 1)
!            call dblepr("crit", 4, crit, 1)

        Else ! df := sum( lev[i] )
          Do i = 1, n
            df = df + lev(i)
          End Do
          If (icrit==3) Then ! df matching --------------------
            crit = 3 + (dofoff-df)**2
          Else ! if(icrit .eq. 4) then df - dofoff (=> zero finding)
            crit = df - dofoff
          End If
        End If
      End If
!     Criterion evaluation
      Return
    End Subroutine sslvrg

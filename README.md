The R Consortium has awarded some modest funding for "histoRicalg",
a project to document and transfer knowledge of some older algorithms 
used by R and by other computational systems. These older codes 
are mainly in Fortran, but some are in C, with the original implementations
possibly in other programming languages. My efforts
were prompted by finding some apparent bugs in codes, which could be either
from the original programs or else the implementations. Two examples
in particular -- in nlm() and in optim::L-BFGS-B -- gave impetus
to the project. 

For active items please refer to [CONTRIBUTING.md](https://gitlab.com/nashjc/histoRicalg/blob/master/CONTRIBUTING.md).

As a first task, I am hoping to establish a "Working Group on 
Algorithms Used in R" to identify and prioritize issues and to
develop procedures for linking older and younger workers to enable
the transfer of knowledge. Expressions of interest are welcome,
either to me (nashjc _at_ uottawa.ca) or to the mailing list 
(https://lists.r-consortium.org/g/rconsortium-project-histoRicalg).
A preliminary web-site is at https://gitlab.com/nashjc/histoRicalg.

While active membership of the Working Group is desirable, given 
the nature of this project, I anticipate that most members will
contribute mainly by providing timely and pertinent ideas. Some 
may not even be R users, since the underlying algorithms are used 
by other computing systems and the documentation effort has many 
common features. We will also need participation of younger 
workers willing to learn about the methods that underly the 
computations in R. 

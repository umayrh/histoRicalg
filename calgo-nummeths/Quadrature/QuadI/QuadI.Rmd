---
title: "QuadI"
author: Oliver Dechant, Dalhousie University
date: "`r format(Sys.time(), '%B %d, %Y')`"
output: 
   pdf_document:
     citation_package: "natbib"
bibliography: QuadI.bib
---
```{r setup, warning=FALSE, message=FALSE, include=FALSE}
require(Rcpp)
source("./src/quadi.R")
```

#Introduction
##The QuadI Algorithm
QuadI appears in a class of quadrature algorithms where the boundary for the derivative of the integrad is known and can be solved exactly. This algorithm provides a convenient helper method for the numerical integration of multiple formula. This can be described as calculating the net area or difference of areas for a function bounded by some definite region.

This document is to be considered a work in progress as part of the histoRicalg project (https://gitlab.com/ nashjc/histoRicalg). Readers are welcome to contribute material.

#Notes on Definite Integrals from @stewart_2016
The area A of the region S that lies under the graph of the continuous function $f$ is the limit of the sum of the areas of approximating rectangles
$$
A=\lim_{n\to\infty}R_{n}=\lim_{n\to\infty}[f(x_{1})\triangle x+f(x_{2})\triangle x+\ldots+f(x_{n})\triangle x],
$$
or using sigma notation
$$
\sum_{i=1}^{n}f(x_{i})\triangle x=f(x_{1})\triangle x+f(x_{2})\triangle x+\ldots+f(x_{n})\triangle x.
$$
So if $f$ is a function defined for $a\leq x\leq b$, we divide the interval $[a,b]$ into $n$ subintervals of equal width $\triangle x=(b-a)/n$ letting $x_{0}(=a),x_{1},x_{2},\ldots,x_{n}(=b)$ be the endpoints of these subintervals and we let $x_{1}^{*},x_{2}^{*},\ldots,x_{n}^{*}$ be any sample points in these subintervals, so $x_{i}^{*}$ lies in the $i$th subinterval $[x_{i-1},x_{i}]$. Thus the definite integral of $f$ from $a$ to $b$ is
$$
\int_{a}^{b}f(x)dx=\lim_{n\to\infty}\sum_{i=1}^{n}f(x_{i}^{*})\triangle x,
$$
provided that this limit exists and gives the same value for all possible choices of sample points. If it does exist we say that $f$ is integrable on $[a,b]$ where
$$
\triangle x=\frac{b-a}{n}\quad\text{and}\quad x_{i}=a+i\triangle x.
$$
Additionally we have the midpoint rule
$$
\int_{a}^{b}f(x)dx\approx\sum_{i=1}^{n}f(\bar x_{i})\triangle x=\triangle x[f(\bar x_{1})+\ldots+f(\bar x_{n})],
$$
where
$$
\bar x=\frac{1}{2}(x_{i-1}+x_{i})=\text{ midpoint of }[x_{i-1},x_{i}].
$$
\newpage

##Test Calculations
```{r}
#quadI()
```
<!--
Suppose we want to approximate $\int_{1}^{2}\frac{1}{x}dx$ with $n=5$.

Simply the endpoint of the five subintervals are: 1, 1.2, 1.4, 1.6, 1.8, and 2.0, so using $\bar x_{i}=\frac{1}{2}(x_{i-1}+x_{i})$ to find the midpoints: 1.1, 1.3, 1.5, 1.7, and 1.9. And the width of the subintervals is $\triangle x=(2-1)/5=\frac{1}{5}$. Now the midpoint rule gives us
$$
\int_{1}^{2}\frac{1}{x}dx\approx\triangle x[f(1.1)+f(1.3)+f(1.5)+f(1.7)+f(1.9)]
$$
$$
=\frac{1}{5}{\huge{(}}\frac{1}{1.1}+\frac{1}{1.3}+\frac{1}{1.5}+\frac{1}{1.7}+\frac{1}{1.9}{\huge{)}}
$$
$$
\approx0.691908
$$
Since $f(x)=1/x>0$ for $1\leq x\leq2$. 
-->
\newpage

#Implementations
##Notes from the Creator @Herbold:1960:AQ:366959.366964 (edited for clarity)
QuadI provides integration for multiple functions of the same limits and point rule at the same time. The finite interval `(a, b)` is divided into `m` equal subintervals for an n-point quadrature integration of the points $x_{i}$ of the formula and weights $w_{i}$ for $i=1,\ldots,n$, based on `p` as the number of functions to be integrated. `w[k]` and `u[k]` (in ascending order) are normalized weights and abscissas respectively, where $k=1,\ldots,n$ and `P(B,j) <- c` evaluates the function `c` as indicated by `j` for `B`. `I[j]` is the result of integration for function `j`.

##R
###`arrayOfFunctions`
```{r R, echo=FALSE, include=TRUE}
arrayOfFunctions
```
###`evaluationFunction`
```{r include=TRUE, echo=FALSE}
evaluationFunction
```

###`quadI`
```{r include=TRUE, echo=FALSE}
quadI
```

```{r R Wrapper}

# Not fully implemented
quadi <- function(a, b, m, n, p, w, u, P) {
  .Call("quadi",a, b, m, n, p, w, u, P)
}
```

##C
Sourced from @Valverde (see ['./src/quadi'](https://gitlab.com/nashjc/histoRicalg/blob/master/nummeths/QuadI/src/quadi)):
```{}
/* The following are functions we want to integrate over */
double function_1(double);
double function_2(double);

/* This array of functions takes a double argument and
 * returns a double value which stores the addresses
 * of the functions */
double (*(array_of_functions[])(double)) = {
                                        function_1,
                                        function_2}
double P(B,j);
double B;
int j;
{
  return (*(array_of_functions[j]))( B );
}
double *QuadI(a, b, m, n, p, w, u, P)
double a, b;  /* interval limits */
int m;        /* number of subintervals */
int n;        /* for an n-point quadrature integration */
double *w;    /* normalized weights: array of n values */
double *u;    /* abscissas: array of n values */
P;            /* function that evaluates c, the function */
              /* (as indicated by j) for B */
{
  double h;         /* subinterval size */
  double *I;        /* Result vector, should be freed by caller */
  double A, B, c;
  int i, j, k;
  
  I = (double *) malloc(n * sizeof double);
  
  h = (b - a) / m;              /* subinterval size */
  for (j = 0; j < p; j++)
    I[j] = 0.0;                 /* initialize results vector */
  A := a - h / 2;
  for (i = 0; i < m; i++) {     
    A += h;
    for (k = 0; k < n; k++) { 
      B := A + (h / 2) * u[k];
      for (j = 0; j < p; j++) {
        c = P(B, j);
        I[j] = I[j] + w[k] * c;
      }
    }
  }
  for (j = 0; j < p; j++) 
    I[j] = (h / 2) * I[j];
  
  return I;
}
```

##Fortran
Code included as './src/quadi.f90'
```{}
  subroutine quadi(a, b, m, n, p, w, u, P)
  implicit none

    
  end program quadi
```

#Comments

JN180620 - Press et al offered rather an uneven collection of codes. Mainly of value because they actually offered codes. This approach dates from 1960, which is REALLY early since Fortran was just barely runnable. Quadrature not my area, but the names James Lyness and David Kahaner were quite prominent (I knew both. Lyness no longer with us. Kahaner may be -- he got out of academic work and became a techno-diplomat to Japan in the 90s) Question: Does this code have an R wrapper?

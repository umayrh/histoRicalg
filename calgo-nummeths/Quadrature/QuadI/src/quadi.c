#include <R.h>

/* The following are functions we want to integrate over */
double function_1(double);
double function_2(double);

/* This array of functions takes a double argument and
 * returns a double value which stores the addresses
 * of the functions */
double (*(array_of_functions[])(double)) = {
                                        function_1,
                                        function_2}
double P(B,j);
double B;
int j;
{
  return (*(array_of_functions[j]))( B );
}
double *QuadI(a, b, m, n, p, w, u, P)
double a, b;  /* interval limits */
int m;        /* number of subintervals */
int n;        /* for an n-point quadrature integration */
double *w;    /* normalized weights: array of n values */
double *u;    /* abscissas: array of n values */
P;            /* function that evaluates c, the function */
              /* (as indicated by j) for B */
{
  double h;         /* subinterval size */
  double *I;        /* Result vector, should be freed by caller */
  double A, B, c;
  int i, j, k;
  
  I = (double *) malloc(n * sizeof double);
  
  h = (b - a) / m;              /* subinterval size */
  for (j = 0; j < p; j++)
    I[j] = 0.0;                 /* initialize results vector */
  A := a - h / 2;
  for (i = 0; i < m; i++) {     
    A += h;
    for (k = 0; k < n; k++) { 
      B := A + (h / 2) * u[k];
      for (j = 0; j < p; j++) {
        c = P(B, j);
        I[j] = I[j] + w[k] * c;
      }
    }
  }
  for (j = 0; j < p; j++) 
    I[j] = (h / 2) * I[j];
  
  return I;
}

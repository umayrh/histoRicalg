---
title: "COLROW and ARCECO: FORTRAN Packages for Solving Certain Almost Block Diagonal Linear Systems by Modified Alternate Row and Column Elimination"
author:
date: "`r format(Sys.time(), '%B %d, %Y')`"
output: 
   pdf_document:
     citation_package: "natbib"
bibliography: COLROWandARCECO.bib
---

\citep{Diaz:1983:ACA:356044.356054}
\citep{Diaz:1988:RLC:45054.356237}

```{r setup, include=TRUE}

```

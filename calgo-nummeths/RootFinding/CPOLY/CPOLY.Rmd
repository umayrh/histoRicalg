---
title: "CPOLY"
author:
- Oliver Dechant, Dalhousie University
date: "`r format(Sys.time(), '%B %d, %Y')`"
output: 
   pdf_document:
     citation_package: "natbib"
bibliography: CPOLY.bib
urlcolor: blue
---
```{r setup, warning=FALSE, message=FALSE, include=FALSE}
source("./src/cpoly.R")
```
@doi:10.1137/0707045 @Jenkins1970 @Jenkins1970:2 @10.2307/2004275

# Introduction

Polynomials are algebraic expression typically described as $\sum_{i=0}^{n}a_{i}x^{i}$ were $a\in\{\mathbb{C}^{+}\text{or }\mathbb{R}^{+}\}$.

Interested readers are encouraged to review the modern implementation of polynomial handling in the R-package \href{https://cran.r-project.org/package=polynom}{polynom}.

## The CPOLY Algorithm
`CPOLY` is method for finding *all* of the zeros of a complex polynomial. It applies a three-stage complex algorithm and is a natural extension of two-stage solvers. A three-stage solver works by finding successive zeros in increasing order of modulus. 

\newpage

# Implementation
## Notes from the Creators @Jenkins:1972:AZC:361254.361262

## Variant of ANSI Fortran
```{r echo=FALSE}
readLines("./src/cpoly.f")
```

## Tests

We managed to run

```
   fort77 cpoly.for
   ./a.out
```

But there were warnings printed during compile. Not yet investigated.